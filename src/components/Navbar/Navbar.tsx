import Image from "next/image";

const Navbar = () => {
  return (
    <div className="container mx-auto mb-8">
      <Image src="/images/logo-dummy.png" alt="Logo" width={161} height={67} />
    </div>
  );
};

export default Navbar;
