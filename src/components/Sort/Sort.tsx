interface SortProps {
  value?: any;
  handleSort?: () => void;
}

const Sort = ({ value, handleSort }: SortProps) => {
  return (
    <div>
      <span>Urutkan</span>
      <select
        className="form-select"
        value={value}
        aria-label="sort"
        onChange={handleSort}
      >
        <option value="new">Terbaru</option>
        <option value="numOfReviews">Review</option>
        <option value="stock">Stock</option>
      </select>
    </div>
  );
};

export default Sort;
