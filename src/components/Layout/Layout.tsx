import { Navbar } from "../Navbar";

type LayoutProps = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div>
      <Navbar />
      <div className="container px-32 mx-auto">{children}</div>
    </div>
  );
};

export default Layout;
