import type { NextPage, GetServerSideProps } from "next";
import request from "../api/request";
import { ResponseData } from "../api/types/response.types";
import { Card, Layout, Sort } from "../components";

type HomepageProps = {
  responseData: ResponseData;
};
const Home: NextPage<HomepageProps> = ({ responseData }) => {
  const { data } = responseData;

  console.log(data, "data");

  return (
    <Layout>
      <div className="flex flex-row">
        <div className="w-1/4 mr-6">
          <h1 className="font-semibold text-color-5 border-b border-slate-50 pb-2">
            Filter
          </h1>
        </div>

        <div className="w-3/4 flex-col ml-6">
          <div className="flex justify-between border-b border-slate-50 pb-2 mb-4">
            <h1 className="font-semibold text-color-5">Product List</h1>
            <Sort />
          </div>

          {/* Component Card */}
          <div className="flex justify-between flex-wrap">
            {data.map((product, index) => {
              return <Card key={product.id} />;
            })}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const { data: response }: { data: ResponseData } = await request("gifts");

  return {
    props: {
      responseData: response,
    },
  };
};

export default Home;
