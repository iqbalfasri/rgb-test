import axios from "axios";

const API_URI = "https://recruitment.dev.rollingglory.com/api/v2/";

const request = axios.create({
  baseURL: API_URI,
  timeout: 5000,
});

export default request;
