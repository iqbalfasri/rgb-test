export interface Meta {
  totalItems: number;
  currentPage: number;
  itemPerPage: number;
  totalPages: number;
}

export interface Attributes {
  id: number;
  name: string;
  info: string;
  description: string;
  points: number;
  slug: string;
  stock: number;
  images: string[];
  isNew: number;
  rating: number;
  numOfReviews: number;
  isWishlist: number;
}

export interface Datum {
  id: string;
  type: string;
  attributes: Attributes;
}

export interface Links {
  self: string;
  next: string;
  last: string;
}

export interface ResponseData {
  meta: Meta;
  data: Datum[];
  links: Links;
}
